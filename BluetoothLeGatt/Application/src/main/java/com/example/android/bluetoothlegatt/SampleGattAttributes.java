/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class SampleGattAttributes {
    private static HashMap<String, String> attributes = new HashMap();
    public static String SERVICE_DEVICE_INFORMATION = "0000ec00-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_ACTIVITY = "0000ec06-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_TEMPS = "0000ec05-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DOSE = "0000ec03-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_CARTRIDGE_CHANGE = "0000ec07-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_CARTRIDGE = "0000ec02-0000-1000-8000-00805f9b34fb";

    public static String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    static {
        attributes.put(CHARACTERISTIC_ACTIVITY, "ACTIVITY");
        attributes.put(CHARACTERISTIC_TEMPS, "TEMPS");
        attributes.put(CHARACTERISTIC_DOSE, "DOSE");
        attributes.put(CHARACTERISTIC_CARTRIDGE_CHANGE, "CARTRIDGE_CHANGE");
        attributes.put(CHARACTERISTIC_CARTRIDGE, "CARTRIDGE");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
